BITS 64
DEFAULT REL

extern printf

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; CpuState must match struct cpu in cpu.h
struc CpuState
	c_reg_a resb 1
	c_reg_b resb 1
	c_reg_c resb 1
	c_reg_d resb 1
	c_reg_e resb 1
	c_reg_h resb 1
	c_reg_l resb 1
	c_status resb 1
	c_reg_sp resw 1
	c_pc resw 1
	resw 2 ; pad
	c_ram resq 1
endstruc

%macro stateload 1
; TODO: copy struct cpu into registers
%endmacro

%macro statesave 1
; TODO: copy registers into struct cpu
%endmacro

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

section .rodata

msg_todo:	db "Instruction not implemented!", 0
msg_nop:	db "NOP instruction", 0
fmt:	db "PC=%3$04lx %1$s (opcode $%2$02lx)", 10, 0

section .rodata

ALIGN 16
jumptable:
	dq ins_nop, ins_x01, ins_x02, ins_x03, ins_x04, ins_x05, ins_x06, ins_x07 ; 00-0F
	dq ins_x08, ins_x09, ins_x0A, ins_x0B, ins_x0C, ins_x0D, ins_x0E, ins_x0F
	dq ins_x10, ins_x11, ins_x12, ins_x13, ins_x14, ins_x15, ins_x16, ins_x17 ; 10-1f
	dq ins_x18, ins_x19, ins_x1A, ins_x1B, ins_x1C, ins_x1D, ins_x1E, ins_x1F
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR ; 20-2f
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR ; 30-3f
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR ; 40-4f
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR ; 50-5f
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR ; 60-6f
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR ; 70-7f
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR ; 80-8f
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR ; 90-9f
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR ; a0-af
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR ; b0-bf
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR ; c0-cf
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR ; d0-df
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR ; e0-ef
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR ; f0-ff
	dq ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR, ins_ERR

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

section .text

; int cpu_init(struct cpu*)
global cpu_init
cpu_init:
	; initialize fields of CpuState
	mov BYTE [rdi + c_reg_a], 0
	mov BYTE [rdi + c_reg_b], 0
	mov BYTE [rdi + c_reg_c], 0
	mov BYTE [rdi + c_reg_d], 0
	mov BYTE [rdi + c_reg_e], 0
	mov WORD [rdi + c_reg_h], 0
	mov BYTE [rdi + c_status], 0
	mov WORD [rdi + c_reg_sp], 0
	mov WORD [rdi + c_pc], 0
	mov QWORD [rdi + c_ram], 0

	; success = 0
	xor eax, eax
	ret

; void cpu_set_ram(struct cpu *cpu, void *ram)
global cpu_set_ram
cpu_set_ram:
	mov QWORD [rdi + c_ram], rsi

	xor eax, eax
	ret

; void cpu_next(struct cpu*)
global cpu_next
cpu_next:
	; access cpu.pc and cpu.ram members
	movzx eax, WORD [rdi + c_pc] ; EAX = cpu.pc
	mov rdx, QWORD [rdi + c_ram] ; RDX = cpu.ram pointer

	; Increment cpu.pc
	lea ecx, [rax + 1]
	mov WORD [rdi + c_pc], cx

	; Fetch opcode
	movzx edx, BYTE [rdx + rax * 1]	; opcode = cpu.ram[cpu.pc]

	; look up jumptable[opcode]
	lea rax, [jumptable]

	; call function_pointer(RDI=cpu, RSI=N/A, RDX=opcode, RCX=PC)
	jmp QWORD [rax + rdx * 8]

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ins_ERR:
	; TODO: set error state

	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	jmp printf wrt ..plt

ins_nop:
	stateload rdi

	mov rdi, fmt
	mov rsi, msg_nop
	xor rax, rax
	call printf wrt ..plt

	; TODO: update tick_debt
	statesave rdi
	ret

ins_x01:
	stateload rdi
	; TODO: implement this

	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x02:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x03:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x04:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x05:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x06:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x07:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x08:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x09:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x0A:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x0B:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x0C:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x0D:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x0E:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x0F:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x10:
	stateload rdi
	; TODO: update tick_debt
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x11:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x12:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x13:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x14:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x15:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x16:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x17:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x18:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x19:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x1A:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x1B:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x1C:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x1D:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x1E:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret

ins_x1F:
	stateload rdi
	; TODO: implement this
	mov rdi, fmt
	mov rsi, msg_todo
	xor rax, rax
	call  printf wrt ..plt

	statesave rdi
	ret
