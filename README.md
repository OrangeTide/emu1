# Emulator I

## Introduction

Emulator for a fantasy game console.

Early screenshot:
![Screen shot #1](docs/images/screenshot1.png)

## Architecture

TBD.

Ideas:
 * dual CPU: Z80 + 6502
 * display: TMS9918A VDP
 * programmable sound generator (PSG): SN76489/YM2149F SSG
 * FM: YM2413 FM
 * gamepad: CD4021 based shift register (NES/SNES style)
 * mouse: SNES style
 * storage: NCR5380 SCSI controller + TEAC FD-235HF 3.5" 1.44MB floppy drive

## Notes

 * TMS9918A VDP ([ColecoVision](https://8bitworkshop.com/blog/platforms/coleco/), MSX1)
  * 4 sprites per scanline
 * [Master System](https://segaretro.org/Sega_Master_System/Technical_specifications) VDP
  * 8 sprites per scanline
 * NES / Famicom PPU
  * 8 sprites per scanline
 * PC Engine / TurboGrafx-16
  * 16 sprites per scanline
 * [Mega Drive / Genesis](https://segaretro.org/Sega_Mega_Drive/Technical_specifications) VDP
  * 20 sprites per scanline
 * Super NES / Super Famicom
  * 32 sprites per scanline

### Supported Platforms

* Linux

## Building, Installation and Prerequisites

### Prerequisite: Raylib

Prerequisites (Ubuntu or Debian):
```sh
sudo apt install libasound2-dev mesa-common-dev libx11-dev libxrandr-dev libxi-dev xorg-dev libgl1-mesa-dev libglu1-mesa-dev
```

Download & Build:
```sh
git clone --depth=2 https://github.com/raysan5/raylib.git raylib
cd raylib/src/
make PLATFORM=PLATFORM_DESKTOP CFLAGS="-flto -Os" # To make the static version.
# make PLATFORM=PLATFORM_DESKTOP RAYLIB_LIBTYPE=SHARED # To make the dynamic shared version.
```

Install System-wide:
```sh
sudo make install # Static version.
# sudo make install RAYLIB_LIBTYPE=SHARED # Dynamic shared version.
```

If you aren't happy:
```sh
sudo make uninstall
sudo make uninstall RAYLIB_LIBTYPE=SHARED
```

*TODO* update documentation for using CMake

### Building

Linux:
```sh
make
```

## Running

Linux:
```sh
./game1
```

## See Also

 * [8-bit Workshop: Write 8-bit code in your browser](https://8bitworkshop.com/)
 * [Ken Shirriff's Blog](http://www.righto.com/)
 * [Thomas Scherrer Z80-Family Official Support Page](http://z80.info/)
 * [Emulator 101](http://www.emulator101.com/)
 * [High Performance Z80 Emulation](https://news.ycombinator.com/item?id=15896336)
 * [System V Application Binary Interface - AMD64](https://refspecs.linuxfoundation.org/elf/x86_64-abi-0.99.pdf)

## License

Pick any one of these:

* No license / PUBLIC DOMAIN
* [CC0](https://creativecommons.org/publicdomain/zero/1.0/)
* [UNLICENSE](https://unlicense.org/UNLICENSE)
* [WTFPL](http://www.wtfpl.net/txt/copying/)
* [0BSD](https://opensource.org/licenses/0BSD)
