#ifndef CPU_H_
#define CPU_H_
#include <stdint.h>

struct cpu {
	uint8_t a, b, c, d, e, h, l;
	uint8_t status;
	uint16_t sp;
	uint16_t pc;
	uint8_t *ram;
};

int cpu_init(struct cpu *cpu);
void cpu_next(struct cpu *cpu);
void cpu_set_ram(struct cpu *cpu, void *ram);
#endif
