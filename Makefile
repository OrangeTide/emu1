all ::
clean :: ; $(RM) $(wildcard *.d)
tests ::
.PHONY : all clean tests
-include $(wildcard *.d)
########################################################################
# Configuration
########################################################################
CFLAGS := -Wall -Os -g
COMMON_CFLAGS := -MMD -MP
RAYLIB_LDLIBS := -lraylib $(shell pkg-config --libs gl x11) -ldl -lrt
RAYLIB_CFLAGS := $(shell pkg-config --cflags gl x11) -DPLATFORM_DESKTOP
########################################################################
# Nasm rules
########################################################################
NASMFLAGS := -felf64 -Wall -g
%.o %.d : %.asm
	nasm $(NASMFLAGS) $(CPPFLAGS) -o $*.o -MD $*.d -MP $<
########################################################################
# Rules
########################################################################
%.o %.d : %.c
	$(CC) $(COMMON_CFLAGS) $(CFLAGS) $(CPPFLAGS) -c -o $*.o $<
########################################################################
# Macros
########################################################################
makeobjs = $(patsubst %.c,%.o,$(patsubst %.cpp,%.o,$(patsubst %.cc,%.o,$(patsubst %.C,%.o,$(patsubst %.S,%.o,$(patsubst %.s,%.o,$(patsubst %.asm,%.o,$1)))))))
# generate rules for a target
# $1 = name of executable
# inputs: SRCS, OBJS, CFLAGS, CPPFLAGS, CXXFLAGS, LDFLAGS, LDLIBS
define generate
$(eval
$1 : $(call makeobjs,$(SRCS)) $(OBJS)
clean :: ; $$(RM) $1 $(call makeobjs,$(SRCS))
$(if $(CFLAGS),$1 : CFLAGS = $(CFLAGS))
$(if $(CPPFLAGS),$1 : CPPFLAGS = $(CPPFLAGS))
$(if $(CXXFLAGS),$1 : CXXFLAGS = $(CXXFLAGS))
$(if $(LDFLAGS),$1 : LDFLAGS = $(LDFLAGS))
$(if $(LDLIBS),$1 : LDLIBS = $(LDLIBS))
CFLAGS := $(DEFAULT_CFLAGS)
CPPFLAGS := $(DEFAULT_CPPFLAGS)
CXXFLAGS := $(DEFAULT_CXXFLAGS)
LDFLAGS := $(DEFAULT_LDFLAGS)
LDLIBS := $(DEFAULT_LDLIBS)
SRCS :=
OBJS :=)
endef
# generate an executable
define generate-exe
$(call generate,$1)
$(eval all :: $1)
endef
# generate a test
define generate-test
$(call generate,$1)
$(eval tests :: $1
	./$1)
endef
########################################################################
# Init
########################################################################
DEFAULT_CFLAGS := $(CFLAGS)
DEFAULT_CPPFLAGS := $(CPPFLAGS)
DEFAULT_CXXFLAGS := $(CXXFLAGS)
DEFAULT_LDFLAGS := $(LDFLAGS)
DEFAULT_LDLIBS := $(LDLIBS)
########################################################################
# Project: emu1
########################################################################
SRCS := emu1.c cpu.asm
CFLAGS = -flto -pthread $(RAYLIB_CFLAGS)
LDFLAGS = -flto -pthread $(RAYLIB_CFLAGS)
LDLIBS = $(RAYLIB_LDLIBS) -lm
# CPPFLAGS = -I/usr/local/include
$(call generate-exe,emu1)
########################################################################
# Project: cpu_test
########################################################################
SRCS := cpu_test.c cpu.asm
$(call generate-test,cpu_test)
