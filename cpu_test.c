#include "cpu.h"
#include <stdio.h>

static void
test(const char *testname, int result)
{
	fprintf(stderr, "%s: %s\n", testname, result ? "PASS" : "FAIL");
}

int
main(int argc, char *argv[])
{
	struct cpu cpu;

	test("CPU INIT", cpu_init(&cpu) == 0);

	cpu_set_ram(&cpu,(uint8_t[]){ 0, 1, 100 }); // TODO: load a program here

	cpu_next(&cpu);
	cpu_next(&cpu);
	cpu_next(&cpu);

	return 0;
}
