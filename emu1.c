/* game1.c : example game using Raylib
 * PUBLIC DOMAIN, CC0, WTFPL, or UNLICENSE. (your choice)
 */
#include <stdbool.h>
#include <stdlib.h>
#include <raylib.h>

#include "cpu.h"

/* Settings: you can change these */
#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
#define VIRTUAL_WIDTH 320
#define VIRTUAL_HEIGHT 240

#if defined(PLATFORM_DESKTOP)
#define GLSL_VERSION	330
#else
#define GLSL_VERSION	100
#endif

static struct {
	short width, height;
	short virtual_width, virtual_height;
	RenderTexture2D target;
	float render_size[2];
	bool scanlines_enabled;
} screen = { SCREEN_WIDTH, SCREEN_HEIGHT };

enum emu_state {
	EMU_STATE_RUN_EMU,
	EMU_STATE_SHOW_MENU,
};

static struct {
	enum emu_state state;
} model = { EMU_STATE_RUN_EMU, };

static struct {
	Shader post_shader; /* post-processing shader */
	int post_shader_render_size_loc; /* location of "renderSize" */
	int post_shader_scanlines_enabled_loc; /* location of "scanlinesEnabled" */
} assets;

static bool emu_done = false;

static struct cpu cpu;

static void
set_scanlines_enabled(bool v)
{
	screen.scanlines_enabled = v;
	SetShaderValue(assets.post_shader, assets.post_shader_scanlines_enabled_loc, &screen.scanlines_enabled, UNIFORM_INT);
}

static void
paint(void)
{
	BeginDrawing();
	ClearBackground(BLACK);

	////////////////////////////////////////////////////////////////////////

	BeginShaderMode(assets.post_shader);

	DrawTexturePro(screen.target.texture,
		(Rectangle){0, 0, screen.virtual_width, -screen.virtual_height},
		(Rectangle){0, 0, screen.width, screen.height},
		(Vector2){ 0, 0 }, 0.f, WHITE);

	EndShaderMode();

	////////////////////////////////////////////////////////////////////////

	EndDrawing();
}

static void
input_update(void)
{
	if (WindowShouldClose())
		emu_done = true;

	if (IsKeyPressed(KEY_R))
		set_scanlines_enabled(!screen.scanlines_enabled);

	switch (model.state) {
	case EMU_STATE_RUN_EMU:
		// if menu button pressed ...
		if (IsKeyPressed(KEY_F1))
			model.state = EMU_STATE_SHOW_MENU;
		break;
	case EMU_STATE_SHOW_MENU:
		if (IsKeyPressed(KEY_ENTER) || IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
			model.state = EMU_STATE_RUN_EMU;
		break;
	}
}

static void
clear_screen(void)
{
	BeginTextureMode(screen.target);
	ClearBackground(BLACK);
	EndTextureMode();
}

static void
emu_run(int ticks)
{
	cpu_next(&cpu);
	cpu.pc = 0; // TODO: overflow check the CPU state

	BeginTextureMode(screen.target);

	int i;
	for (i = 0; i < ticks; i++) {
		// TODO: implement an emulator
		int x = rand() % VIRTUAL_WIDTH;
		int y = rand() % VIRTUAL_HEIGHT;
		Color c = { rand() % 256, rand() % 256, rand() % 256, 255 };

		DrawPixel(x, y, c);

	}
	EndTextureMode();
}

static void
load_assets(void)
{
	assets.post_shader = LoadShader(0, TextFormat("assets/shaders/glsl%i/raster.fs", GLSL_VERSION));

	/* set the vec2 renderSize in the shader */
	assets.post_shader_render_size_loc = GetShaderLocation(assets.post_shader, "renderSize");
	SetShaderValue(assets.post_shader, assets.post_shader_render_size_loc, screen.render_size, UNIFORM_VEC2);
	assets.post_shader_scanlines_enabled_loc = GetShaderLocation(assets.post_shader, "scanlinesEnabled");
	set_scanlines_enabled(true);
}

static void
init(void)
{
	InitWindow(screen.width, screen.height, "Game I");

	SetTargetFPS(60);

	screen.render_size[0] = screen.width;
	screen.render_size[1] = screen.height;
	screen.virtual_width = VIRTUAL_WIDTH;
	screen.virtual_height = VIRTUAL_HEIGHT;
	screen.target = LoadRenderTexture(screen.virtual_width, screen.virtual_height);

	load_assets();
}

static void
done(void)
{
	UnloadRenderTexture(screen.target);
	CloseWindow();
}

int
main(int argc, char *argv[])
{
	cpu_init(&cpu);
	cpu_set_ram(&cpu, (uint8_t[]) { 0, 0, 0, 0, 0, 0, 0, 0 }); // TODO: load from ROM
	init();

	clear_screen();
	while (!emu_done) {
		input_update();
		emu_run(20);
		paint();
	}

	done();

	return 0;
}
