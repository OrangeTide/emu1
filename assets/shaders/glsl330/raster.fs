#version 330

// Input vertex attributes (from vertex shader)
in vec2 fragTexCoord;
in vec4 fragColor;

// Input uniform values
uniform sampler2D texture0;
uniform vec4 colDiffuse;

// Output fragment color
out vec4 finalColor;

// NOTE: Add here your custom variables

uniform bool scanlinesEnabled = true;
uniform vec2 renderSize; // required!
float offset = 0.0;

uniform float time;

void main()
{
    vec4 texelColor = texture(texture0, fragTexCoord);

    if (scanlinesEnabled) {
        float frequency = renderSize.y / 3.0;
        float globalPos = (fragTexCoord.y + offset) * frequency;
        float wavePos = cos((fract(globalPos) - 0.5) * 3.141592);

        finalColor = mix(vec4(0.0, 0.3, 0.0, 0.0), texelColor, wavePos);
    } else {
        finalColor = texelColor;
    }
}
